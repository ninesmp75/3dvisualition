# 3D Visualisation

[[_TOC_]]

## Description

A executable that extracts information of a FITS cube [ra, dec, velocity] and creates a x3d file for visualisation.

### <a name="parameters"></a> Inputs

* **FITSFILE**. String defining the path of FITS file (<span style="color:blue">mandatory</span>). Example: '~/dir/name.fits'
* **isolevels**. 1D float array (<span style="color:blue">optional</span>)).The values of the datacube from which to create isosurfaces (unit = Jy/beam). Example: 0.1 0.25 0.5 0.8. Default=None.
* **limit_pos**. 2x2 float array (<span style="color:blue">optional</span>)). Limits in ra and dec for the cube (units=deg). Example: [[330, 332], [-28, -10000]]. Default=None (all the cube).
* **limit_veloc**. 1x2 float array (<span style="color:blue">optional</span>)). Limits in velocity for the cube (unit=m/s). Example: 6600000 7700000. Default=None (all the cube).
* **step**. Step size (int) to estimate the model (<span style="color:blue">optional</span>)). Higher step makes lower resolution. Default is 1.
* **style**. String defining the Visualisation style (<span style="color:blue">optional</span>)). Choose between "transparent"/"opaque". Default="transparent".
* **namex3d**. String defining the name of x3d file WITHOUT extension (<span style="color:blue">optional</span>)). Example:"~/dir/namex3d'.Default: same that FITS cube.

### Ouput
* x3d file


## How to run the task

The run.sh script will pull the latest container from the registry (no building is required), and execute the task.

    run.sh

## Directory structure
<pre>
│
├── scripts
│       │
│       ├── fits_x3d
│       │
│       └── pipeline.sh
│
├── Dockerfile
│
├── README.md
│
├── requeriments.tx
│
└── run.sh
</pre>


## Dependencies
* matplotlib
* scikit-image
* json
* astropy==5.2.2
* astroquery
* spectral_cube


## Usage (see [parameters](#parameters))

    fits_x3d --help

    usage: fits_x3d.py [-h] [--isolevels ISOLEVELS [ISOLEVELS ...]] [--lim_pos [[330.33,333.45], [-29.15,-26.44]]] [--lim_veloc LIM_VELOC LIM_VELOC] [--step {1,2,3,4,5,6,7,8,9,10,11,12,13,14}]
                   [--style transparent/opaque] [--namex3d pathOUT/namex3d]
                   pathIN/namefits

    Create file .x3d from fits

    positional arguments:
    pathIN/namefits       full path of Fits file. e.g.: "~/dir/name.fits

    options:
    -h, --help            show this help message and exit
    --isolevels ISOLEVELS [ISOLEVELS ...]
                        The values of the datacube from which to create isosurfaces. e.g. 0.1 0.25 0.5 0.8
    --lim_pos [[330.33,333.45], [-29.15,-26.44]]
                        Limits the range of RA and DEC. e.g. [[330.33,333.45], [-29.15,-26.44]]
    --lim_veloc LIM_VELOC LIM_VELOC
                        Limits the range of velocity. e.g. 6500000.1 7750000.3.
    --step {1,2,3,4,5,6,7,8,9,10,11,12,13,14}
                        Step size (int) to estimate the model. Higher step makes lower resolution. Default is 1
    --style transparent/opaque
                        Grating setup. Default=transparent
    --namex3d pathOUT/namex3d
                        full path of the output x3d file. e.g.: "~/dir/namex3d

### Examples

* `python3 fits_x3d.py HCG91.fits`

* `python3 fits_x3d.py HCG91.fits --isolevels 0.1 0.25 0.5 0.8 --lim_pos "[[330, 332], [-28, -10000]]" --lim_veloc 6600000 7700000 --step 10 --style opaque --name HCG91vis`









