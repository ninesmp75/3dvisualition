#!/bin/bash

# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/3dvisualization:latest

# run container pipeline
docker run -it --rm --name 3dvisualization -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/3dvisualization:latest

