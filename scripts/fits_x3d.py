#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue Jan 23
@author: ixakalabadie & MA
"""

import argparse
import json
import numpy as np
from skimage import measure
from matplotlib import cm
import astropy.units as u
from astropy.coordinates import Angle, SkyCoord
from astropy import wcs
from spectral_cube import SpectralCube
from astroquery.ipac.ned import Ned


class MakeAll:

    """
    Class to create a X3D model of iso-surfaces with 3D spectral line data.
    """

    def __init__(self, path, isolevels=None, lim_pos=None, lim_veloc=None,
                 units=None, image2d=None, galaxies=None):
        """

        Create the X3D and HTML files all at once.

        Parameters
        ----------
        path : string
            Path to FITS file containing the data cube to be visualized.
        isolevels : array-like, optional
            The values of the datacube from which to create isosurfaces, no
            maximum length (not tested big numbers). Must be in the same units
            given by the "units" parameter, but the array itself dimensionless.
            If None, four layers will be created automatically.
            The default is None
        lims_pos : 2D array, optional
                Limits the range of RA and DEC, to make a cutout.
                The default is None.
        lim_veloc: 1D array, optional
                   Limits the range of veloc, to make a cutout.
                   The default is None.
        image2d : tuple, optional
            A two-element tuple. The first element is a string with the name of
            a survey and the second is the dimenson of the image in pixels.
            For example: ('DSS2 Blue', '200x200'). The default is None.

        units : astropy unit, optional
            The unit for the values in the data cube, does not have to be the
            same the FITS file has. Also works in string form.
            The default is None. If want to leave some units as default set to
            -1, e.g. (-1, 'deg', -1,'Angs')
        galaxies : array-like
            Array of strings with the names of the galaxies to represent, to be
            searched in NED.

        Returns
        -------
        None.
        """

        cube = SpectralCube.read(path)
        if cube.ndim != 3:
            raise Exception("Not enough axes")

        cubehead = cube.header
        self.obj = cubehead['OBJECT']
        nz, ny, nx = cube.shape
        cubeunits = [cubehead['BUNIT'], cubehead['CUNIT1'], cubehead['CUNIT2'],
                     cubehead['CUNIT3']]
        delta = [cubehead['CDELT1'], cubehead['CDELT2'], cubehead['CDELT3']]

        # Set the limits of the cube
        if lim_pos is not None:
            lim_pos = (lim_pos*u.Unit('deg')).to(cubeunits[1])

        if lim_veloc is not None:
            lim_veloc = (lim_veloc*u.Unit('m/s')).to(cubeunits[3])

        # limit the cube
        mask, ralim, declim, vlim = limit_cube(cube, lim_pos, lim_veloc)

        cube = cube.subcube_from_mask(mask)

        coords = np.array([ralim, declim, vlim])
        print(coords)

        angular_units = ['arcsec', 'arcmin', 'deg', 'rad']
        astropy_prefixes = ['', 'k', 'm', 'M', 'u', 'G', 'n', 'd', 'c', 'da',
                            'h', 'p', 'T', 'f', 'a', 'P', 'E', 'z', 'y', 'Z',
                            'Y', 'r', 'q', 'R', 'Q']

        if cubeunits[0] == 'JY/BEAM':
            cubeunits[0] = 'Jy/beam'

        if units is not None:
            for i in range(len(units)):
                if units[i] == '-1':
                    units[i] = cubeunits[i]
        else:
            units = cubeunits.copy()
            diffcoords = get_coords(coords[0], coords[1], coords[2])[1][:, 2]
            for ax in range(3):
                j = 0
                while 10 > diffcoords[ax] or diffcoords[ax] > 10000:
                    if u.Unit(units[ax+1]).is_equivalent(u.rad):
                        diffcoords[ax] = diffcoords[ax] * \
                            u.Unit(units[ax+1]).to(angular_units[j])
                        units[ax+1] = angular_units[j]
                    elif u.Unit(units[ax+1]).is_equivalent(u.m/u.s):
                        diffcoords[ax] = diffcoords[ax] * \
                            u.Unit(units[ax+1]).to(astropy_prefixes[j]+'m/s')
                        units[ax+1] = astropy_prefixes[j]+'m/s'
                    elif u.Unit(units[ax+1]).is_equivalent(u.Hz):
                        diffcoords[ax] = diffcoords[ax] * \
                            u.Unit(units[ax+1]).to(astropy_prefixes[j]+'Hz')
                        units[ax+1] = astropy_prefixes[j]+'Hz'
                    j = j+1
            print("Automatic units = "+str(units))

        delta = np.array(
            [delta[0]*u.Unit(cubeunits[1]).to(units[1]),
             delta[1]*u.Unit(cubeunits[2]).to(units[2]),
             delta[2]*u.Unit(cubeunits[3]).to(units[3])])

        cube = cube.unmasked_data[:, :, :]
        cube = cube.to_value()

        i = 0
        while np.max(cube) < 1:
            i = i+1
            cube = cube*10
        if i < 10:
            units[0] = "e0%s %s" % (i, units[0])
        elif i > 0:
            units[0] = "e%s %s" % (i, units[0])

        cube = transpose(cube, delta)

        if isolevels is None:
            isolevels = calc_isolevels(cube)
            print("Automatic isolevels = "+str(isolevels))
        else:
            isolevels.sort()
            if (np.any(isolevels) > cube.max() or
                    np.any(isolevels) < cube.min()):
                isolevels = calc_isolevels(cube)
                print("Isolevels out of range. Use automatic isolevels = "
                      + str(isolevels))

        self.color = create_colormap('CMRmap', isolevels)

        if image2d == -1:
            # CREATE INDEXEDSURFACE FOR A PLANE, JUST NEED FOUR POINTS
            self.imcol = None
            self.img_shape = None
        elif image2d is not None:
            survey, pixels = image2d
            verts = (coords[0, 0], coords[0, 1], coords[1, 0], coords[1, 1])
            print("Downloading image...")
            self.imcol, self.img_shape, _ = get_imcol(position=self.obj,
                                                      survey=survey,
                                                      verts=verts, unit='deg',
                                                      pixels=pixels,
                                                      coordinates='J2000',
                                                      grid=True,
                                                      gridlabels=True)
            # Allow using kwargs in get_imcol
            print("Done.")

        self.galdict = None
        try:
            if galaxies == ['query']:
                # query a region from NED -> gives velocity
                sc = SkyCoord(coords[0][0]*u.Unit(cubeunits[1]),
                              coords[1][0]*u.Unit(cubeunits[2]))
                sepa = SkyCoord(
                    np.mean(coords[0])*u.Unit(cubeunits[1]),
                    np.mean(coords[1])*u.Unit(cubeunits[2])).separation(sc)
                result = Ned.query_region(
                    self.obj, radius=sepa)['Object Name', 'Type', 'RA', 'DEC',
                                           'Velocity']
                if result['RA'].unit == 'degrees':
                    result['RA'].unit = u.deg
                if result['DEC'].unit == 'degrees':
                    result['DEC'].unit = u.deg
                result = objquery(result, [
                    coords[0]*u.Unit(cubeunits[1]),
                    coords[1]*u.Unit(cubeunits[2]),
                    coords[2]*u.Unit(cubeunits[3])], otype='G')
                self.galdict = {}
                for gal in result:
                    galcoords = SkyCoord(ra=gal['RA']*result['RA'].unit,
                                         dec=gal['DEC']*result['DEC'].unit)
                    galra = (galcoords.ra-np.mean(
                        coords[0])*u.Unit(cubeunits[1])) \
                        * np.cos(coords[1][0]*u.Unit(cubeunits[2]).to('rad'))
                    galdec = (galcoords.dec -
                              np.mean(coords[1])*u.Unit(cubeunits[2]))
                    galv = gal['Velocity']*result['Velocity'].unit - \
                        np.mean(coords[2])*u.Unit(cubeunits[3])
                    self.galdict[gal['Object Name']] = {
                        'coord': np.array(
                            [galra.to(u.Unit(units[1])).to_value(),
                             galdec.to(u.Unit(units[2])).to_value(),
                             galv.to(u.Unit(units[3])).to_value()]),
                        'col': '0 0 1'}

            elif galaxies is not None:
                # "Galaxies" is a list of names separated by commas.
                # Query each one and get its coordinates and velocity.
                self.galdict = {}
                for gal in galaxies:
                    result = Ned.query_object(gal)
                    if result['RA'].unit == 'degrees':
                        result['RA'].unit = u.deg
                    if result['DEC'].unit == 'degrees':
                        result['DEC'].unit = u.deg
                    ra = float(result['RA'])*result['RA'].unit
                    dec = float(result['DEC'])*result['DEC'].unit
                    v = float(result['Velocity'])*result['Velocity'].unit
                    galra = (ra-np.mean(coords[0]*u.Unit(cubeunits[1]))) \
                        * np.cos(coords[1][0]*u.Unit(cubeunits[2]).to('rad'))
                    galdec = dec-np.mean(coords[1]*u.Unit(cubeunits[2]))
                    galv = v-np.mean(coords[2]*u.Unit(cubeunits[3]))
                    self.galdict[gal] = {
                        'coord': np.array(
                            [galra.to(u.Unit(units[1])).to_value(),
                             galdec.to(u.Unit(units[2])).to_value(),
                             galv.to(u.Unit(units[3])).to_value()]),
                        'col': '0 0 1'}

        except Exception as ex:
            self.galdict = None
            print(ex)
            print('Model will be generated without galaxies')

        self.delta = np.abs(delta)
        self.coords = coords
        self.cube = cube
        self.isolevels = isolevels
        self.hdr = cubehead
        if image2d is not None:
            self.x3dim2d = True
        else:
            self.x3dim2d = False
        self.cubehead = cubehead
        self.cubeunits = cubeunits
        self.units = units

    def makeX3D(self, path=None, meta=None, style='transparent', step_size=1,
                picking=False):
        """
        Create the X3D  files.

        Parameters
        ----------
        path : string
            Path to where the files will be saved, including the name of the
            files but not the extension, e.g. '~/username/data/somecube'.
            If None, it will create the files in the working directory.
        meta : string
            Something. Default is None.

        Returns
        -------
        Creates an X3D file and an HTML file in path.

        """
        if path is None:
            path = self.obj
        file = write_x3d(path+'.x3d', delta=self.delta, header=self.hdr,
                         units=self.units, coords=self.coords, meta=meta,
                         picking=picking, style=style)

        l_cubes = self.cube
        if not isinstance(l_cubes, list):
            l_cubes = [l_cubes]
            l_isolevels = [self.isolevels]

        file.make_layers(l_cubes, l_isolevels, self.color,
                         step_size=step_size)
        file.make_outline()
        if self.x3dim2d:
            file.make_image2d(self.imcol, self.img_shape)
        if self.galdict is not None:
            file.make_galaxies(self.galdict)
        file.make_ticklines()
        file.make_animation()
        file.make_labels(gals=self.galdict, axlab='real')

        file.close()


class write_x3d:

    """
    Class to create a X3D model of iso-surfaces with 3D spectral line data.
    Creates an X3D file with the model.

    Parameters
    ----------
    filename : string, optinal
        Name of the file to be created. Should have the extension '.x3d'.
    delta : len 3 array
        Array with the step in each direction of the cube.
        Better if they are all around the same order and between 0.1~10.
    coords : astropy.Quantity 2d array
        Array with the minimum and maximum of the RA, DEC and VRAD
        in each row, in that order, of the cube. Must be an astropy quantity.
    header: Astropy cube header
    units : astropy unit, optional
            The unit for the values in the data cube, does not have to be the
            same the FITS file has. Also works in string form.
            The default is None
    meta: string, metada
    style: painting style
    picking: Boolean (False or True)

    Returns
    -------
    None.

    """

    def __init__(self, filename, delta, coords, header, units, meta=None,
                 picking=False, style='transparent'):
        self.delta = delta
        self.hdr = header
        self.units = units
        self.style = style  # style can be 'transparent' or 'opaque'
        # set labels and outline black or white depending on style
        if self.style == 'transparent':
            self.col = '0 0 0'
        if self.style == 'opaque':
            self.col = '1 1 1'
        self.real_coords, self.diff_coords = get_coords(
            coords[0], coords[1], coords[2])
        self.diff_coords[0] = self.diff_coords[0] * \
            u.Unit(header["CUNIT1"]).to(units[1])
        self.diff_coords[1] = self.diff_coords[1] * \
            u.Unit(header["CUNIT2"]).to(units[2])
        self.real_coords[2] = self.real_coords[2] * \
            u.Unit(header["CUNIT3"]).to(units[3])
        self.diff_coords[2] = self.diff_coords[2] * \
            u.Unit(header["CUNIT3"]).to(units[3])
        print(self.diff_coords)
        if picking:
            picking = 'true'
        else:
            picking = 'false'
        self.file_x3d = open(filename, 'w')
        self.file_x3d.write('<?xml version="1.0" encoding="UTF-8"?>\n '
                            '<!DOCTYPE X3D PUBLIC "ISO//Web3D//DTD X3D '
                            '3.3//EN" \n "http://'
                            'www.web3d.org/specifications/x3d-3.3.dtd">')
        self.file_x3d.write(
            '\n <X3D profile="Immersive" version="3.3" xmlns:xsd="http://'
            'www.w3.org/2001/XMLSchema-instance" '
            'xsd:noNamespaceSchemaLocation="http://'
            'www.web3d.org/specifications/x3d-3.3.xsd">')
        self.file_x3d.write(
            '\n <head>\n\t<meta name="file" content="%s"/>' % filename)
        # Additional metadata
        if meta is not None:
            for met in meta.keys():
                self.file_x3d.write(
                    '\n\t<meta name="%s" content="%s"/>' % (met, meta[met]))
        self.file_x3d.write(
            '\n </head>\n\t<Scene doPickPass="%s">\n\t\t<Background DEF="back"'
            ' skyColor="0.6 0.6 0.6"/>' % picking)
        self.file_x3d.write(
            '\n\t\t<NavigationInfo type=\'"EXAMINE" "ANY"\' speed="4" '
            'headlight="true"/>')
        self.file_x3d.write(
            '\n\t\t<DirectionalLight ambientIntensity="1" '
            'intensity="0" color="1 1 1"/>')
        self.file_x3d.write('\n\t\t<Transform DEF="ROOT" translation="0 0 0">')

    def make_layers(self, l_cubes, l_isolevels, l_colors, shifts=None,
                    step_size=1, add_normals=False):
        # create function to set step size automatically
        """
        Makes layers of the equal intensity and writes an x3d file

        The shapes of the cubes, if many, must be the same

        Parameters
        ----------
       l_cube : list of 3d arrays
            The cubes to plot. Axis must be RA, DEC and spectral axis.
            If more than one cube, the shape of all cubes must be the same,
            just set unknown or unwanted values to 0 (OR NAN?).
        l_isolevels : list of arrays
            A list of arrays with the values of each isosurface layer, one
            array for each cube.
            E.g.[2,5,9] for three layers at values 2, 5 and 9 of the cube.
            Should be in increasing order. (REAL?)
        l_colors : list of larrays
            RGB color for each isolevel a string ('122 233 20').
            There must be a list of colors for each cube.
        shift : list, optional
            A list with a arrays of 3D vectors giving the shift in RA, DEC and
            spectral axis in the same units given to the cube.
            Similar to l_cube or l_isolevels.



        Returns
        -------
        None.

        """

        numcubes = len(l_cubes)
        self.iso_split = []
        for nc in range(numcubes):
            cube_full = l_cubes[nc]
            isolevels = l_isolevels[nc]
            self.iso_split.append(np.zeros((len(isolevels)), dtype=int))
            for i in range(len(isolevels)):

                """ calculate how many times to split the cube,
                1 means the cube stays the same """
                split = int(np.sum(cube_full > isolevels[i])/700000) + 1
                nx, ny, nz = cube_full.shape

                for sp in range(split):

                    cube = cube_full[:, :, int(
                        nz/split*sp):int(nz/split*(sp+1))]
                    mins = (self.diff_coords[0][0], self.diff_coords[1][0],
                            self.diff_coords[2][0]*(1-2*sp/split))

                    try:
                        if shifts is not None:
                            verts, faces, normals = marching_cubes(
                                cube, level=isolevels[i], delta=self.delta,
                                mins=mins, shift=shifts[nc],
                                step_size=step_size)
                        else:
                            verts, faces, normals = marching_cubes(
                                cube, level=isolevels[i], delta=self.delta,
                                mins=mins, step_size=step_size)

                        self.file_x3d.write('\n'+tabs(3) +
                                            '<Transform DEF="%slt%s_sp%s" '
                                            'translation="0 0 0" '
                                            'rotation="0 0 1 -0" '
                                            'scale="1 1 1">\n' % (nc, i, sp))
                        self.file_x3d.write(tabs(4) + '<Shape DEF='
                                            '"%slayer%s_sp%s_shape">\n' %
                                            (nc, i, sp))
                        self.file_x3d.write(tabs(5) +
                                            '<Appearance sortKey="%s">\n' %
                                            (len(isolevels)-1-i))
                        if self.style == 'transparent':
                            # set color and transparency of layer
                            if i == len(isolevels)-1:
                                op = 0.4
                            else:
                                op = 0.8
                            self.file_x3d.write('\n' + tabs(6) + '<Material '
                                                'DEF="%slayer%s_sp%s" '
                                                'ambientIntensity="0" '
                                                'emissiveColor="0 0 0" '
                                                'diffuseColor="%s" '
                                                'specularColor="0 0 0" '
                                                'shininess="0.0078" '
                                                'transparency="%s"/>' %
                                                (nc, i, sp, l_colors[nc][i],
                                                 op))
                        elif self.style == 'opaque':
                            # set color of layer, transparency is set in HTML
                            self.file_x3d.write('\n' + tabs(6) + '<Material '
                                                'DEF="%slayer%s_sp%s" '
                                                'ambientIntensity="0" '
                                                'emissiveColor="%s" '
                                                'diffuseColor="%s" '
                                                'specularColor="0 0 0" '
                                                'shininess="0.0078"/>' %
                                                (nc, i, sp, l_colors[nc][i],
                                                 l_colors[nc][i]))
                        # correct color with depthmode (ALSO FOR LAST LAYER?)
                        # if i != len(isolevels)-1:
                        self.file_x3d.write('\n' + tabs(6) + '<DepthMode '
                                            'readOnly="true"></DepthMode>')
                        self.file_x3d.write('\n' + tabs(5) + '</Appearance>')
                        # define the layer object
                        if add_normals:
                            self.file_x3d.write('\n' + tabs(5) +
                                                '<IndexedFaceSet '
                                                'solid="false" '
                                                'colorPerVertex="false" '
                                                'normalPerVertex="true" '
                                                'coordIndex="\n\t\t\t\t\t\t')
                        else:
                            self.file_x3d.write('\n' + tabs(5) +
                                                '<IndexedFaceSet '
                                                'solid="false" '
                                                'colorPerVertex="false" '
                                                'normalPerVertex="false" '
                                                'coordIndex="\n\t\t\t\t\t\t')
                        # write indices
                        np.savetxt(self.file_x3d, faces, fmt='%i',
                                   newline=' -1\n\t\t\t\t\t\t')
                        self.file_x3d.write('">\n')
                        self.file_x3d.write(tabs(6) + '<Coordinate '
                                            'DEF="%sCoordinates%s_sp%s" '
                                            'point="\n\t\t\t\t\t\t' %
                                            (nc, i, sp))
                        # write coordinates
                        np.savetxt(self.file_x3d, verts, fmt='%.3f',
                                   newline=',\n\t\t\t\t\t\t')
                        self.file_x3d.write('"/>\n')
                        if add_normals:
                            self.file_x3d.write(tabs(6) + '<Normal '
                                                'DEF="%sNormals%s_sp%s" '
                                                'vector="\n\t\t\t\t\t\t' %
                                                (nc, i, sp))
                            # write normals
                            np.savetxt(self.file_x3d, normals, fmt='%.5f',
                                       newline=',\n\t\t\t\t\t\t')
                            self.file_x3d.write('"/>\n')
                        self.file_x3d.write(tabs(5)+'</IndexedFaceSet>\n')
                        self.file_x3d.write(tabs(4)+'</Shape>\n')
                        self.file_x3d.write(tabs(3)+'</Transform>\n')
                    except Exception as ex:
                        print(ex)

    def make_outline(self):
        """
        Creates an object for an outline in the X3D file.
        """
        ramin, _, ramax = self.diff_coords[0]
        decmin, _, decmax = self.diff_coords[1]
        vmin, _, vmax = self.diff_coords[2]
        outlinecoords = np.array([[ramin, decmin, vmin],
                                  [ramax, decmin, vmin],
                                  [ramin, decmax, vmin],
                                  [ramax, decmax, vmin],
                                  [ramin, decmin, vmax],
                                  [ramax, decmin, vmax],
                                  [ramin, decmax, vmax],
                                  [ramax, decmax, vmax]])
        # Create outline
        self.file_x3d.write(
            '\n\t\t\t<Transform DEF="ot" translation="0 0 0" '
            'rotation="0 0 1 -0" scale="1 1 1">')
        self.file_x3d.write('\n\t\t\t\t<Shape ispickable="false">')
        self.file_x3d.write('\n\t\t\t\t\t<Appearance>')
        # define ouline ID
        self.file_x3d.write(
            '\n\t\t\t\t\t\t<Material DEF="outline" emissiveColor="%s" '
            'diffuseColor="0 0 0"/>' % self.col)
        self.file_x3d.write('\n\t\t\t\t\t</Appearance>')
        self.file_x3d.write(
            '\n\t\t\t\t\t<IndexedLineSet colorPerVertex="false" '
            'coordIndex="\n\t\t\t\t\t\t')
        # write indices
        np.savetxt(self.file_x3d, outlineindex,
                   fmt='%i', newline='\n\t\t\t\t\t\t')
        self.file_x3d.write('">')
        self.file_x3d.write(
            '\n\t\t\t\t\t\t<Coordinate DEF="OutlineCoords" '
            'point="\n\t\t\t\t\t\t')
        # write coordinates
        np.savetxt(self.file_x3d, outlinecoords,
                   fmt='%.3f', newline=',\n\t\t\t\t\t\t')
        self.file_x3d.write('"/>')
        self.file_x3d.write(
            '\n\t\t\t\t\t</IndexedLineSet>\n\t\t\t\t</Shape>'
            '\n\t\t\t</Transform>\n')

    def make_galaxies(self, gals):
        """
        Creates spheres and labels in the model at the location of galaxies
        given as input.

        Parameters
        ----------
        gals : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        ramin1, _, ramax1 = self.diff_coords[0]
        decmin1, _, decmax1 = self.diff_coords[1]
        vmin1, _, vmax1 = self.diff_coords[2]
        self.mar = np.min([ramax1-ramin1, decmax1-decmin1, vmax1-vmin1])

        sphereradius = self.mar/45  # min(self.delta)*8
        crosslen = self.mar/20  # min(self.delta)*20
        # create galaxy crosses and spheres
        for i, gal in enumerate(gals.keys()):
            # galaxy crosses
            self.file_x3d.write(
                tabs(3) + '<Transform DEF="%s_cross_tra" translation="0 0 0" '
                'rotation="0 0 1 -0" scale="1 1 1">\n' % gal)
            self.file_x3d.write(
                tabs(4)+'<Shape ispickable="false">\n')
            self.file_x3d.write(
                tabs(5)+'<Appearance>\n')
            self.file_x3d.write(
                tabs(6) + '<Material DEF="%s" emissiveColor="%s" '
                'diffuseColor="0 0 0"/>\n' % (gal+'_cross', self.col))
            self.file_x3d.write(
                tabs(5)+'</Appearance>\n')
            # cross indices
            self.file_x3d.write(tabs(5) + '<IndexedLineSet '
                                'colorPerVertex="true" coordIndex="\n' +
                                tabs(6) + '0 1 -1\n' + tabs(6) + '2 3 -1\n' +
                                tabs(6) + '4 5 -1\n'+tabs(6)+'">\n')
            self.file_x3d.write(
                tabs(5) + '<Coordinate DEF="CrossCoords%s" '
                'point="\n\t\t\t\t\t\t' % i)
            vec = gals[gal]['coord']
            crosscoords = np.array([[vec[0]-crosslen, vec[1], vec[2]],
                                    [vec[0]+crosslen, vec[1], vec[2]],
                                    [vec[0], vec[1]-crosslen, vec[2]],
                                    [vec[0], vec[1]+crosslen, vec[2]],
                                    [vec[0], vec[1], vec[2]-crosslen],
                                    [vec[0], vec[1], vec[2]+crosslen]])
            # cross coordinates
            np.savetxt(self.file_x3d, crosscoords,
                       fmt='%.3f', newline='\n\t\t\t\t\t\t')
            self.file_x3d.write(tabs(6) + '"/>\n')
            self.file_x3d.write(tabs(3) +
                                '</IndexedLineSet>\n\t\t\t\t</Shape>\n\t\t\t'
                                '</Transform>\n')
            # galaxy spheres (ADD SCALE, ROTATION, ETC.??)
            self.file_x3d.write(tabs(3) + '<Transform DEF="%s_sphere_tra" '
                                'translation="%s %s %s">\n' %
                                (gal, vec[0], vec[1], vec[2]))
            self.file_x3d.write(tabs(4) + '<Shape ispickable="false">\n')
            self.file_x3d.write(tabs(5) +
                                '<Sphere radius="%s" solid="false"/>\n' %
                                sphereradius)
            self.file_x3d.write(tabs(5) + '<Appearance>\n')
            self.file_x3d.write(tabs(6) + '<Material DEF="%s" '
                                'ambientIntensity="0" emissiveColor="0 0 0" '
                                'diffuseColor="%s" specularColor="0 0 0" '
                                'shininess="0.0078" transparency="0"/>\n' %
                                (gal, gals[gal]['col']))
            self.file_x3d.write(tabs(5) +
                                '</Appearance>\n\t\t\t\t</Shape>\n\t\t\t'
                                '</Transform>\n')

    def make_image2d(self, imcol=None, img_shape=None):
        """
        Create a 2d image in the X3D figure.

        Parameters
        ----------
        imcol : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        ramin, _, ramax = self.diff_coords[0]
        decmin, _, decmax = self.diff_coords[1]
        _, _, vmax = self.diff_coords[2]

        # coordinates of 2d image
        coords2d = np.array([[ramax, decmin, vmax],
                             [ramax, decmax, vmax],
                             [ramin, decmin, vmax],
                             [ramin, decmax, vmax]])

        self.file_x3d.write(tabs(3) + '<Transform DEF="image2d" '
                            'translation="0 0 0" rotation="0 0 1 -0" '
                            'scale="1 1 1">\n')
        self.file_x3d.write(tabs(4) + '<Shape ispickable="False">\n')
        self.file_x3d.write(tabs(5) + '<Appearance>\n')
        self.file_x3d.write(tabs(6) + '<Material DEF="immat" '
                            'ambientIntensity="1" emissiveColor="0 0 0" '
                            'diffuseColor="1 1 1" shininess="0.0078"/>\n')
        if imcol is not None and img_shape is not None:
            self.file_x3d.write(tabs(6) + '<PixelTexture repeatS="false" '
                                'repeatT="false" image=" %s %s 3 \n' %
                                (img_shape[0], img_shape[1]))
            # write pixel colors
            np.savetxt(self.file_x3d, imcol, fmt='%s',
                       delimiter=' ', newline='\n')
            self.file_x3d.write('"/>\n')
        self.file_x3d.write(tabs(5)+'</Appearance>\n')
        # SOLID=TRUE makes it transparent from one side
        self.file_x3d.write(tabs(5) + '<IndexedFaceSet solid="false" '
                            'colorPerVertex="false" normalPerVertex="false" '
                            'coordIndex="2 3 1 0 -1">\n')
        self.file_x3d.write(
            tabs(6)+'<Coordinate DEF="imgCoords" point="\n\t\t\t\t\t\t')
        # write coordinates
        np.savetxt(self.file_x3d, coords2d, fmt='%.3f',
                   newline='\n\t\t\t\t\t\t')
        self.file_x3d.write('"/>\n')
        self.file_x3d.write(tabs(6) + '<TextureCoordinate DEF="imgTexCoords" '
                            'point="\n' + tabs(6) + ' 0 0, 1 0, 0 1, 1 1"/>\n')
        self.file_x3d.write(tabs(5) + '</IndexedFaceSet>\n')
        self.file_x3d.write(tabs(4) + '</Shape>\n')
        self.file_x3d.write(tabs(3) + '</Transform>\n')

    def make_ticklines(self):
        """

        Create tickline objects in the X3D model.
        Must be change to close the Transform "ROOT" somewhere else,
        in case this is not used.
        Change method to end ROOT with new module structure
        """
        ramin, ramean, ramax = self.diff_coords[0]
        decmin, decmean, decmax = self.diff_coords[1]
        vmin, vmean, vmax = self.diff_coords[2]
        # coordinates of tick lines
        ticklinecoords = np.array([[ramin, decmean, vmin],
                                   [ramax, decmean, vmin],
                                   [ramean, decmin, vmin],
                                   [ramean, decmax, vmin],
                                   [ramin, decmean, vmin],
                                   [ramin, decmean, vmax],
                                   [ramin, decmin, vmean],
                                   [ramin, decmax, vmean],
                                   [ramean, decmax, vmin],
                                   [ramean, decmax, vmax],
                                   [ramin, decmax, vmean],
                                   [ramax, decmax, vmean]])
        # Create ticklines
        self.file_x3d.write(tabs(3) + '<Transform DEF="tlt" '
                            'translation="0 0 0" '
                            'rotation="0 0 1 -0" '
                            'scale="1 1 1">\n')
        self.file_x3d.write(tabs(4) + '<Shape ispickable="false">\n')
        self.file_x3d.write(tabs(5) + '<Appearance>\n')
        # set color
        self.file_x3d.write(tabs(6) + '<Material DEF="ticklines"  '
                            'emissiveColor="%s" '
                            'diffuseColor="0 0 0"/>\n' % self.col)
        self.file_x3d.write(tabs(5) + '</Appearance>\n')
        self.file_x3d.write(tabs(5) + '<IndexedLineSet colorPerVertex="false" '
                            'coordIndex="\n\t\t\t\t\t\t')
        # write indices
        np.savetxt(self.file_x3d, ticklineindex, fmt='%i',
                   newline='\n\t\t\t\t\t\t')
        self.file_x3d.write('">\n')
        self.file_x3d.write(tabs(6) + '<Coordinate DEF="ticklineCoords" '
                            'point="\n\t\t\t\t\t\t')
        # write coordinates
        np.savetxt(self.file_x3d, ticklinecoords, fmt='%.3f',
                   newline=',\n\t\t\t\t\t\t')
        self.file_x3d.write('"/>\n')
        self.file_x3d.write(tabs(5) + '</IndexedLineSet>\n')
        self.file_x3d.write(tabs(4) + '</Shape>\n')
        self.file_x3d.write(tabs(3) + '</Transform>\n')
        self.file_x3d.write(tabs(2) + ' </Transform>\n')

    def make_animation(self, cycleinterval=10, axis=0):
        """
        Create an animation to rotate the X3D model along one axis.

        Must be outside the Transform "ROOT" element (for now write after
        make_ticklines).
        """
        vec = np.zeros(3, dtype=int)
        vec[axis] = 1
        vec = str(vec)[1:-1]
        self.file_x3d.write(
            '\n' + tabs(2) + '<timeSensor DEF="time" cycleInterval="%s" '
            'loop="true" enabled="true" startTime="-1"></timeSensor>' %
            cycleinterval)
        self.file_x3d.write(
            '\n' + tabs(2)+'<OrientationInterpolator DEF="move" key="0 0.5 1" '
            'keyValue="%s 0  %s 3.14  %s 6.28"/>' % (vec, vec, vec))
        self.file_x3d.write(
            '\n' + tabs(2)+'<Route fromNode="time" '
            'fromField ="fraction_changed" toNode="move" '
            'toField="set_fraction"></Route>')
        self.file_x3d.write(
            '\n' + tabs(2)+'<Route fromNode="move" fromField ="value_changed"'
            ' toNode="ROOT" toField="rotation"></Route>')

    def make_labels(self, gals=None, axlab=None):
        """
        Create the labels of different elements in the figure.

        Parameters
        ----------
        gals : dictionary, optional
            Dictionary with the names of the galaxies as keys and another
            dictionary inside them with: 'v': the radio velocity,
            'col': the RGB color and 'coord' the spatial coordinates with the
            same format as the rest of the elements.
            If None galaxy labels are not created. The default is None.
        axlab : string, optional
            A string indicating what ax labels to include. Can be 'real' for
            the actual coordinates; 'diff' for the difference from the center
            of the cube or 'both' for both options, in this case ???.
            Leave None for no ax labels. The default is None.

        Returns
        -------
        None.

        """
        self.file_x3d.write(
            '\n\t\t<ProximitySensor DEF="PROX_LABEL" '
            'size="1.0e+06 1.0e+06 1.0e+06"/>')
        self.file_x3d.write('\n\t\t<Collision enabled="false">')
        self.file_x3d.write('\n\t\t\t<Transform DEF="TRANS_LABEL">')

        ramin1, _, ramax1 = self.diff_coords[0]
        decmin1, _, decmax1 = self.diff_coords[1]
        vmin1, _, vmax1 = self.diff_coords[2]
        ramin2, _, ramax2 = Angle(
            self.real_coords[0] * u.Unit('deg')).to_string(u.hour, precision=0)
        decmin2, _, decmax2 = Angle(
            self.real_coords[1] * u.Unit('deg')).to_string(u.degree,
                                                           precision=0)
        vmin2, _, vmax2 = self.real_coords[2]

        # scale of labels
        shape = np.min([ramax1-ramin1, decmax1-decmin1, vmax1-vmin1])
        labelscale = calc_scale(shape)

        ax, axtick = labpos(self.diff_coords)

        # Names for the axes tick labels
        axticknames1 = np.array([f'{ramax1:.2f}', f'{ramin1:.2f}',
                                 f'{decmax1:.2f}', f'{decmin1:.2f}',
                                 f'{vmin1:.0f}', f'{vmax1:.0f}',
                                 f'{decmax1:.2f}', f'{decmin1:.2f}',
                                 f'{vmin1:.0f}', f'{vmax1:.0f}',
                                 f'{ramax1:.2f}', f'{ramin1:.2f}'])

        axticknames2 = np.array([ramax2, ramin2, decmax2,
                                 decmin2, f'{vmin2:.0f}', f'{vmax2:.0f}',
                                 decmax2, decmin2, f'{vmin2:.2f}',
                                 f'{vmax2:.0f}', ramax2, ramin2])

        # galaxy labels
        if gals:
            for (i, gal) in enumerate(gals.keys()):
                self.file_x3d.write('\n\t\t\t\t<Transform DEF="glt%s" '
                                    'translation="%s %s %s" '
                                    'rotation="0 1 0 3.14" scale="%s %s %s">' %
                                    (i, gals[gal]['coord'][0],
                                     gals[gal]['coord'][1],
                                     gals[gal]['coord'][2], labelscale,
                                     labelscale, labelscale))
                self.file_x3d.write(
                    '\n\t\t\t\t\t<Billboard axisOfRotation="0,0,0"  '
                    'bboxCenter="0,0,0">')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t<Shape ispickable="false">\n\t\t\t\t\t\t'
                    '<Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t<Material DEF="%s" diffuseColor="0 0 0" '
                    'emissiveColor="%s"/>' % ('label_'+gal, self.col))
                self.file_x3d.write('\n\t\t\t\t\t\t</Appearance>')
                self.file_x3d.write('\n\t\t\t\t\t\t<Text string="%s">' % gal)
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<FontStyle DEF="%s_fs" family=\'"SANS"\' '
                    'topToBottom="false" justify=\'"BEGIN" "BEGIN"\' '
                    'size="8"/>' % gal)
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t</Text> \n\t\t\t\t\t\t</Shape>\n\t\t\t\t\t'
                    '</Billboard>\n\t\t\t\t</Transform>')

        axlabnames = get_axlabnames(head=self.hdr, units=self.units)

        # CHANGE this, all in same for
        if axlab == 'diff' or axlab == 'both':
            # ax labels
            for i in range(6):
                self.file_x3d.write('\n\t\t\t\t<Transform DEF="alt_diff%s" '
                                    'translation="%s %s %s" rotation="%s" '
                                    'scale="%s %s %s">' %
                                    (i, ax[i, 0], ax[i, 1], ax[i, 2],
                                     axlabrot[i], labelscale, labelscale,
                                     labelscale))
                self.file_x3d.write(
                    '\n\t\t\t\t\t<Shape ispickable="false">\n\t\t\t\t\t\t'
                    '<Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<Material DEF="axlab_diff%s" '
                    'diffuseColor="0 0 0" emissiveColor="%s"/>' %
                    (i, self.col))
                self.file_x3d.write('\n\t\t\t\t\t\t</Appearance>')
                self.file_x3d.write(
                    "\n\t\t\t\t\t\t<Text string='%s'>" % axlabnames[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<FontStyle family=\'"SANS"\' '
                    'topToBottom="false" justify=\'%s\' size="10"/>' %
                    axlabeljustify[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t</Text>\n\t\t\t\t\t</Shape>\n\t\t\t\t'
                    '</Transform>')
            # ax tick labels
            for i in range(12):
                if i < 4:
                    rot = axlabrot[0]
                elif i < 8:
                    rot = axlabrot[2]
                else:
                    rot = axlabrot[4]
                self.file_x3d.write('\n\t\t\t\t<Transform DEF="att_diff%s" '
                                    'translation="%s %s %s" rotation="%s" '
                                    'scale="%s %s %s">' %
                                    (i, axtick[i, 0], axtick[i, 1],
                                     axtick[i, 2], rot, labelscale, labelscale,
                                     labelscale))
                self.file_x3d.write(
                    '\n\t\t\t\t\t<Shape ispickable="false">\n\t\t\t\t\t\t'
                    '<Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<Material DEF="axtick_diff%s" '
                    'diffuseColor="0 0 0" emissiveColor="%s"/>' %
                    (i, self.col))
                self.file_x3d.write('\n\t\t\t\t\t\t</Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t<Text string="%s">' % axticknames1[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<FontStyle family=\'"SANS"\' '
                    'topToBottom="false" justify=\'%s\' size="8"/>' %
                    axticklabjus[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t</Text>\n\t\t\t\t\t</Shape>\n\t\t\t\t'
                    '</Transform>')

        if axlab == 'real' or axlab == 'both':
            if axlab == 'real':
                trans = '0'
            if axlab == 'both':
                trans = '1'
            # ax labels
            for i in range(6):
                self.file_x3d.write('\n\t\t\t\t<Transform DEF="alt_real%s" '
                                    'translation="%s %s %s" rotation="%s" '
                                    'scale="%s %s %s">' %
                                    (i, ax[i, 0], ax[i, 1], ax[i, 2],
                                     axlabrot[i], labelscale, labelscale,
                                     labelscale))
                self.file_x3d.write(
                    '\n\t\t\t\t\t<Shape ispickable="false">\n\t\t\t\t\t\t'
                    '<Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<Material DEF="axlab_real%s" '
                    'diffuseColor="0 0 0" emissiveColor="%s" '
                    'transparency="%s"/>' % (i, self.col, trans))
                self.file_x3d.write('\n\t\t\t\t\t\t</Appearance>')
                self.file_x3d.write(
                    "\n\t\t\t\t\t\t<Text string='%s'>" % axlabnames[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<FontStyle family=\'"SANS"\' '
                    'topToBottom="false" justify=\'%s\' size="10"/>' %
                    axlabeljustify[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t</Text>\n\t\t\t\t\t</Shape>\n\t\t\t\t'
                    '</Transform>')
            # ax tick labels
            for i in range(12):
                if i < 4:
                    rot = axlabrot[0]
                elif i < 8:
                    rot = axlabrot[2]
                else:
                    rot = axlabrot[4]
                self.file_x3d.write('\n\t\t\t\t<Transform DEF="att_real%s" '
                                    'translation="%s %s %s" rotation="%s" '
                                    'scale="%s %s %s">' %
                                    (i, axtick[i, 0], axtick[i, 1],
                                     axtick[i, 2], rot, labelscale, labelscale,
                                     labelscale))
                self.file_x3d.write(
                    '\n\t\t\t\t\t<Shape ispickable="false">\n\t\t\t\t\t\t'
                    '<Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<Material DEF="axtick_real%s" '
                    'diffuseColor="0 0 0" emissiveColor="%s" '
                    'transparency="%s"/>' % (i, self.col, trans))
                self.file_x3d.write('\n\t\t\t\t\t\t</Appearance>')
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t<Text string="%s">' % axticknames2[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t\t<FontStyle family=\'"SANS"\' '
                    'topToBottom="false" justify=\'%s\' size="8"/>' %
                    axticklabjus[i])
                self.file_x3d.write(
                    '\n\t\t\t\t\t\t</Text>\n\t\t\t\t\t</Shape>\n\t\t\t\t'
                    '</Transform>')

        self.file_x3d.write('\n\t\t\t</Transform>')
        self.file_x3d.write('\n\t\t</Collision>')

    def close(self):
        """
        Closes the X3D file. Not using this function at the end results in
        an error.

        Fix collision when only layers are plotted (no labels)

        Returns
        -------
        None.

        """
        # ending, close all
        self.file_x3d.write(
            '\n\t\t<ROUTE fromNode="PROX_LABEL" fromField="position_changed" '
            'toNode="TRANS_LABEL" toField="set_translation"/>')
        self.file_x3d.write(
            '\n\t\t<ROUTE fromNode="PROX_LABEL" '
            'fromField="orientation_changed" toNode="TRANS_LABEL" '
            'toField="set_rotation"/>')
        self.file_x3d.write('\n\t</Scene>')
        self.file_x3d.write('\n</X3D>')
        self.file_x3d.close()


# Some miscellaneoues functions

def limit_cube(cube, lim_pos, lim_veloc):
    ''' limit the size of cube

    Parameters
    ----------
    Inputs:
    cube : 3D array
    lim_ra : 1D array of two elements, min ra, max ra to take account
    lim_dec: 1D array of two elements, min dec, max dec to take account
    lim_veloc: 1D array of two elements, min and max velocity to take account
    Inputs:
    mask : 3D array, mask cube to take account
    ralim : 1D array of two elements, min ra, max ra to use
    declim: 1D array of two elements, min dec, max dec to use
    vlim: 1D array of two elements, min and max velocity to use
    '''

    values_ra = cube.spatial_coordinate_map[1]
    values_dec = cube.spatial_coordinate_map[0]
    values_veloc = cube.spectral_axis

    ra_max = np.max(values_ra)
    ra_min = np.min(values_ra)
    dec_max = np.max(values_dec)
    dec_min = np.min(values_dec)
    if lim_pos is not None:

        lim_ra = lim_pos[0]
        lim_dec = lim_pos[1]

        ra_max = min(ra_max, np.max(lim_ra))
        ra_min = max(ra_min, np.min(lim_ra))

        dec_max = min(dec_max, np.max(lim_dec))
        dec_min = max(dec_min, np.min(lim_dec))

    veloc_max = np.max(values_veloc)
    veloc_min = np.min(values_veloc)
    if lim_veloc is not None:
        veloc_max = min(veloc_max, np.max(lim_veloc))
        veloc_min = max(veloc_min, np.min(lim_veloc))

    mask_ra = (values_ra >= ra_min) & (values_ra <= ra_max)
    mask_dec = (values_dec >= dec_min) & (values_dec <= dec_max)
    mask_veloc = (values_veloc >= veloc_min) & (values_veloc <= veloc_max)

    mask = mask_ra * mask_dec * mask_veloc.reshape((-1, 1, 1))

    ralim = [ra_min.to_value(), ra_max.to_value()]
    declim = [dec_min.to_value(), dec_max.to_value()]
    vlim = [veloc_min.to_value(), veloc_max.to_value()]

    return mask, ralim, declim, vlim


def marching_cubes(cube, level, delta, mins, shift=(0, 0, 0), step_size=1):
    """

    Parameters
    ----------

    cube : 3D array
        Datacube.
    level : float
        Value of the isosurface.
    delta : tuple
        Spacing in each dimension.
    mins : tuple
        Minimum values of each dimension.
    shift : tuple, optional
        Shift in RA, DEC and V in same units as delta and mins.
        The default is (0,0,0).
    step_size : int, optional
        Step size for the marching_cubes algorithm. Set the resolution.
        Default is 1.

    Returns:
    Tuple with
    (1) Array with the coordinates of the vertices of the create triangular
    faces and
    (2) the indices for those faces and (3) normal vectors for each face.
    """
    ramin, decmin, vmin = mins
    verts, faces, normals, _ = measure.marching_cubes(cube, level=level,
                                                      spacing=delta,
                                                      allow_degenerate=False,
                                                      step_size=step_size)
    """ spacing gives the spacing in each dimension.
    # we multiply by the sign to have the coordinates
    # in increasing order, same as cube"""
    return np.array([verts[:, 0] + ramin+shift[0],
                     verts[:, 1] + decmin+shift[1],
                     verts[:, 2] + vmin+shift[2]]).T, \
        faces, \
        np.array([normals[:, 0], normals[:, 1], normals[:, 2]]).T


def calc_scale(shape):
    """
    Function to calculate the scale for labels
    Parameters
    ----------
    shape : float
        Minimum between the difference in RA, DEC and spectral axis of the cube
        shape = np.min([ramax1-ramin1, decmax1-decmin1, vmax1-vmin1])

    Returns
    -------
    scale : float
        Value for the X3D scale parameter of the labels.
    """
    # scale = 0.71096782*np.sqrt(shape)-3.84296963 #sqrt
    # scale = 0.02985932*shape-0.16425599 #linear
    # scale = 3.72083418*np.log(shape)-19.84129672 #logarithmic
    scale = shape*0.01  # linear, seems best (ORIGINALLY 0.01)
    # if scale < 1:
    #    scale = 1
    return scale


def create_colormap(colormap, isolevels, start=0, end=255, lightdark=False):
    """
    Function to create a colormap for the iso-surfaces.

    Parameters
    ----------
    colormap : string
        Name of a matplotlib colormap.
    isolevels : list
        List of values of the iso-surfaces.
    start : int, optional
        Starting element of the colormap array. Default is 0.
    end : int, optional
        Ending element of the colormap array. Default is 255.
    lightdark : bool, optional
        Wheter to reverse the colormap if the darkest side is at the beggining

    Returns
    -------
    cmap : list
        List of strings with the colors of the colormap in the format 'r g b'.
    """
    colors = cm.get_cmap(colormap)(range(256))[:, :-1]
    if lightdark:
        if np.sum(colors[0]) < np.sum(colors[-1]):
            colors = colors[::-1]
    cmap = []
    for i in range(len(isolevels)):
        m = (end-start)/(np.max(isolevels)-np.min(isolevels))
        pos = int((m*isolevels[i]-m*np.min(isolevels))+start)
        cmap.append(
            f'{colors[pos][0]:.5e} {colors[pos][1]:.5e} {colors[pos][2]:.5e}')
    return cmap


def get_coords(ra, dec, v):
    ra_real = np.array([ra[0], np.mean(ra), ra[1]])
    dec_real = np.array([dec[0], np.mean(dec), dec[1]])
    v_real = np.array([v[0], np.mean(v), v[1]])

    ramin = ((ra[0]-np.mean(ra))*np.cos(np.radians(dec[0])))
    ramax = ((ra[1]-np.mean(ra))*np.cos(np.radians(dec[0])))
    decmin = (dec[0]-np.mean(dec))
    decmax = (dec[1]-np.mean(dec))
    vmin = (v[0]-np.mean(v))
    vmax = (v[1]-np.mean(v))

    ra_diff = np.array([ramin, 0, ramax])
    dec_diff = np.array([decmin, 0, decmax])
    v_diff = np.array([vmin, 0, vmax])

    return np.array([ra_real, dec_real, v_real]), np.array([ra_diff, dec_diff,
                                                            v_diff])


def tabs(n):
    return '\t'*n


def calc_isolevels(cube):
    """
    Function to calculate isolevels if not given by the user.

    Parameters
    ----------
    cube : 3D array
        Datacube.
    """
    if np.min(cube) < 0:
        isolevels = [np.max(cube)/10., np.max(cube)/5.,
                     np.max(cube)/3., np.max(cube)/1.5]
    elif np.min(cube) < np.max(cube)/5.:
        isolevels = [np.min(cube), np.max(cube)/5.,
                     np.max(cube)/3., np.max(cube)/1.5]
    return isolevels


def objquery(result, coords, otype):
    """
    Constrain query table to certain coordinates and object type
    """
    result = result[result['Type'] == otype]
    result = result[result['Velocity'] >= coords[2][0]]
    result = result[result['Velocity'] <= coords[2][1]]
    result = result[result['RA'] >= coords[0][0]]
    result = result[result['RA'] <= coords[0][1]]
    result = result[result['DEC'] >= coords[1][0]]
    result = result[result['DEC'] <= coords[1][1]]
    return result


def labpos(coords):
    ramin1, _, ramax1 = coords[0]
    decmin1, _, decmax1 = coords[1]
    vmin1, vmean1, vmax1 = coords[2]
    ax = np.array([[0, decmin1*1.1, vmin1],
                   [ramax1*1.1, 0, vmin1],
                   [ramin1, decmin1*1.1, vmean1],
                   [ramin1, 0, vmin1*1.1],
                   [ramin1*1.1, decmax1, vmean1],
                   [0, decmax1, vmin1*1.1]])
    axtick = np.array([[ramax1, decmin1*1.1, vmin1],
                       [ramin1, decmin1*1.1, vmin1],
                       [ramax1*1.1, decmax1, vmin1],
                       [ramax1*1.1, decmin1, vmin1],
                       [ramin1, decmin1*1.1, vmin1],
                       [ramin1, decmin1*1.1, vmax1],
                       [ramin1, decmax1, vmin1*1.1],
                       [ramin1, decmin1, vmin1*1.1],
                       [ramin1*1.1, decmax1, vmin1],
                       [ramin1*1.1, decmax1, vmax1],
                       [ramax1, decmax1, vmin1*1.1],
                       [ramin1, decmax1, vmin1*1.1]])
    return ax, axtick


def get_imcol(position, survey, verts, unit='deg', cmap='Greys', **kwargs):
    """
    Downloads an image from astroquery and returns the colors of the pixels
    using a certain colormap, in hexadecimal format, as required by
    'write_x3d().make_image2d'.
    See astroquery.skyview.SkyView.get_images() for more information.

    Parameters
    ----------
    position : string or SkyCoord
        Name of an object or it position coordinates.
    survey : string
        Survey from which to make the query.
        See astroquery.skyview.SkyView.list_surveys().
    verts : array
        Minimum RA, maximum RA, minimum DEC and maximum DEC of the data cube,
        in that order.
    **kwargs :
        Other parameters for astroquery.skyview.SkyView.get_images().
        Useful parameters are 'unit', 'pixels' and 'coordinates'.

    Returns
    -------
    imcol : array
        Array with the colors of each pixel in hexadecimal format.
    shape : tuple
        Shape of the image.
    img : array
        Image data.

    """
    from astroquery.skyview import SkyView
    import matplotlib.colors as colors

    try:
        img = SkyView.get_images(position=position, survey=survey, **kwargs)[0]
        imhead = img[0].header
        img = img[0].data
        imw = wcs.WCS(imhead)

        try:
            verts = verts.to('deg')
        except AttributeError:
            verts = verts * u.Unit(unit)

        ll_ra, ll_dec = imw.world_to_pixel(SkyCoord(verts[0], verts[2]))
        lr_ra, _ = imw.world_to_pixel(SkyCoord(verts[1], verts[2]))
        _, ul_dec = imw.world_to_pixel(SkyCoord(verts[0], verts[3]))
        if ll_ra < 0 or ll_dec < 0 or lr_ra < 0 or ul_dec < 0:
            print('ERROR: The image is smaller than the cube. '
                  'Increase parameter "pixels"')
            print("Pixel indices for [ra1, dec1, ra2, dec2] = " +
                  str([ll_ra, ll_dec, lr_ra, ul_dec]) +
                  ". Set 'pixels' parameter higher than the difference "
                  "between 1 and 2.")
            raise ValueError
        img = img[int(ll_dec):int(ul_dec), int(lr_ra):int(ll_ra)]
        # dec first, ra second!!

        shape = img.shape

        img = img-np.min(img)
        img = (img)/np.max(img)

        colimg = cm.get_cmap(cmap)(img)[:, :, 0:3]
        colimg = colimg.reshape((-1, 3), order='F')

        imcol = [colors.rgb2hex(c).replace('#', '0x') for c in colimg]
        if len(imcol) % 8 == 0:
            imcol = np.array(imcol).reshape(int(len(imcol)/8), 8)

    except AttributeError:

        imcol = None
        shape = None
        img = None

    return imcol, shape, img


def transpose(array, delta):
    dra, ddec, dv = delta
    return np.transpose(array, (2, 1, 0))[::int(np.sign(dra)),
                                          ::int(np.sign(ddec)),
                                          ::int(np.sign(dv))]


def get_axlabnames(head, units):
    return np.array([head['CTYPE1'].split('-')[0]+' ('+units[1]+')',
                     head['CTYPE2'].split('-')[0]+' ('+units[2]+')',
                     head['CTYPE3'].split('-')[0]+' ('+units[3]+')',
                     head['CTYPE2'].split('-')[0]+' ('+units[2]+')',
                     head['CTYPE3'].split('-')[0]+' ('+units[3]+')',
                     head['CTYPE1'].split('-')[0]+' ('+units[1]+')'])


# Some attributes for the classes and functions
ticklineindex = np.array([[0, 1, -1],
                          [2, 3, -1],
                          [4, 5, -1],
                          [6, 7, -1],
                          [8, 9, -1],
                          [10, 11, -1]])
outlineindex = np.array([[0, 1, -1],
                         [2, 3, -1],
                         [4, 5, -1],
                         [6, 7, -1],
                         [0, 2, -1],
                         [1, 3, -1],
                         [4, 6, -1],
                         [5, 7, -1],
                         [0, 4, -1],
                         [1, 5, -1],
                         [2, 6, -1],
                         [3, 7, -1]])

# justification of axes labels
axlabeljustify = np.array(['"MIDDLE" "END"', '"MIDDLE" "BEGIN"',
                           '"MIDDLE" "END"', '"MIDDLE" "BEGIN"',
                           '"MIDDLE" "END"', '"MIDDLE" "BEGIN"'])
# justification of axes tick labels
axticklabjus = np.array(['"MIDDLE" "END"', '"MIDDLE" "END"',
                         '"END" "END"', '"END" "BEGIN"',
                         '"MIDDLE" "END"', '"MIDDLE" "END"',
                         '"END" "END"', '"END" "BEGIN"',
                         '"MIDDLE" "END"', '"MIDDLE" "END"',
                         '"END" "END"', '"END" "BEGIN"'])
# rotation of ax labels
axlabrot = np.array(['0 1 0 3.14', '1 1 0 3.14', '0 1 0 -1.57',
                     '1 1 -1 -2.0944', '1 1 1 -2.0944', '1 0 0 -1.57'])


def parseSTYLE(style):
    l_style = ['transparent', 'opaque']
    if style not in l_style:
        raise argparse.ArgumentTypeError("incorrect STYLE")
    return style


# main
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create file .x3d from fits')
    parser.add_argument('FITSFILE', type=str, metavar='pathIN/namefits',
                        help='full path of Fits file. e.g.: "~/dir/name.fits')
    parser.add_argument('--isolevels', nargs='+', type=float, default=None,
                        help='The values of the datacube from which to create '
                        'isosurfaces. e.g. 0.1 0.25 0.5 0.8')
    parser.add_argument('--lim_pos', type=json.loads, default=None,
                        metavar='[[330.33,333.45], [-29.15,-26.44]]',
                        help='Limits the range of RA and DEC '
                        'e.g.  [[330.33,333.45], [-29.15,-26.44]]')
    parser.add_argument('--lim_veloc', nargs=2, type=float, default=None,
                        help='Limits the range of velocity. '
                        'e.g. 6500000.1 7750000.3')
    parser.add_argument('--step', type=int, choices=range(1, 15), default=1,
                        help='Step size (int) to estimate the model. '
                        'Higher step makes lower resolution. Default is 1')
    parser.add_argument('--style', metavar='transparent/opaque',
                        type=parseSTYLE, default='transparent',
                        help='Grating setup. Default=transparent',)
    parser.add_argument('--namex3d', type=str, metavar='pathOUT/namex3d',
                        help='full path of the output x3d file. '
                        'e.g.: "~/dir/namex3d')
    args = parser.parse_args()

    fitsfile = args.FITSFILE
    isolevels = args.isolevels
    lim_pos = args.lim_pos
    lim_veloc = args.lim_veloc
    step_size = args.step
    namex3d = args.namex3d
    style = args.style

    cube = MakeAll(fitsfile, isolevels=isolevels, lim_pos=lim_pos,
                   lim_veloc=lim_veloc)
    cube.makeX3D(path=namex3d, style=style, step_size=step_size)
