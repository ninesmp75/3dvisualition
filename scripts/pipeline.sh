#!/bin/bash
# this script fixes permissions inside the docker container,
# downloads data, and runs a python fits2x3d script.

# fix permissions
chmod 755 * -R

# download data
#wget https://....../HG91.fits

# run the code
python3 fits_x3d.py HCG91.fits
